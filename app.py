from flask import Flask,render_template
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

class Task(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	text = db.Column(db.Text)
	complete = db.Column(db.Boolean, default=False)
  
@app.route('/')
def index():
	return render_template('index.html')
	
@app.route('/mohan')
def index1():
	return render_template('mohan.html')
	
if __name__ == '__main__':
	app.run(debug=True)
